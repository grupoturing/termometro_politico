import numpy as np
import pandas as pd
import tweepy
import seaborn as sns
import textblob
from IPython.display import display
from credential import * # used to access twitter api
from textblob import TextBlob
import re
from matplotlib import pyplot

#------------------------------------------------------------------#
                         #GET TWEETS#
#------------------------------------------------------------------#

def twitter_setup():
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
    api = tweepy.API(auth)
    return api

#Tweets extraction
extractor = twitter_setup()

#Create a list with 1000 tweets 
tweets = extractor.user_timeline(screen_name="jairbolsonaro", count=1000)
print("Number of tweets extracted: {}.\n".format(len(tweets)))

#Print 5 recent tweets
print("5 recent tweets:\n")
for tweet in tweets[:5]:
    print(tweet.text)
    print()

data = pd.DataFrame(data=[tweet.text for tweet in tweets], columns=['Tweets'])
display(data.head(10))

print(tweets[0].id)
print(tweets[0].created_at)
print(tweets[0].source)
print(tweets[0].favorite_count)
print(tweets[0].retweet_count)
print(tweets[0].geo)
print(tweets[0].coordinates)
print(tweets[0].entities)

#Numpy for statistical analysis...
data['len'] = np.array([len(tweet.text) for tweet in tweets])
data['ID'] = np.array([tweet.id for tweet in tweets])
data['Date'] = np.array([tweet.created_at for tweet in tweets])
data['Source'] = np.array([tweet.source for tweet in tweets])
data['Likes'] = np.array([tweet.favorite_count for tweet in tweets])
data['RTs'] = np.array([tweet.retweet_count for tweet in tweets])

#------------------------------------------------------------------#
                         #FIRST ANALYSIS#
#------------------------------------------------------------------#
mean = np.mean(data['len'])
print("The lenght's average in tweets: {}".format(mean))

fav_max = np.max(data['Likes'])
rt_max = np.max(data['RTs'])
 
fav = data[data.Likes == fav_max].index[0]
rt = data[data.RTs == rt_max].index[0]
 
# Max FAVs:
print("The tweet with more likes is: \n{}".format(data['Tweets'][fav]))
print("Number of likes: {}".format(fav_max))
print("{} characters.\n".format(data['len'][fav]))
 
# Max RTs:
print("The tweet with more retweets is: \n{}".format(data['Tweets'][rt]))
print("Number of retweets: {}".format(rt_max))
print("{} characters.\n".format(data['len'][rt]))

tlen = pd.Series(data=data['len'].values, index=data['Date'])
tfav = pd.Series(data=data['Likes'].values, index=data['Date'])
tret = pd.Series(data=data['RTs'].values, index=data['Date'])

#Size of tweets over time
tlen.plot(figsize=(16,4), color='r')
#pyplot.show()

tfav.plot(figsize=(16,4), label="Likes", legend=True)
#pyplot.show()

tret.plot(figsize=(16,4), label="Retweets", legend=True)
#pyplot.show()

#Observe correlations (!!!!!!!!!)
corr = data.corr()
sns.heatmap(corr,xticklabels=corr.columns.values,yticklabels=corr.columns.values)
#pyplot.show()

#------------------------------------------------------------------#
                         #TERM FREQUENCY#
#------------------------------------------------------------------#
import nltk
from nltk import word_tokenize 
from nltk.probability import FreqDist
from string import punctuation
import json
import unidecode

stopwords = set(nltk.corpus.stopwords.words('portuguese'))

def remove_accents(phrase):
    accented_string = str(phrase)
    unaccented_string = unidecode.unidecode(accented_string).lower()
    return unaccented_string

def cut_stopwords(tokens):
    non_stop = []
    for token in tokens:
        for p in list(punctuation):
            token = token.replace(p,'')
        if token not in stopwords:
            non_stop.append(token)
    return non_stop    

def get_alpha(tokens):
    clean_tokens = []
    for token in tokens:
        if token.isalpha():
            clean_tokens.append(token)
    return clean_tokens


class myDict(dict):
    def __init__(self):
        self = dict()
    def add(self, key, value):
        self[key] = value

all_tokens = myDict()

def colect_tokens(tokens):
    for token in tokens:
        #value = int(all_tokens.keys()) + 1
        value = 1
        all_tokens.add(token, value)

print(all_tokens)

#Preparing and cleaning
data['Tweets'] = data.Tweets.apply(lambda x: remove_accents(x)).astype(str) 
data['tokens'] = data.Tweets.apply(lambda x: word_tokenize(str(x).lower(), language='portuguese'))
data['alpha_tokens'] = data.tokens.apply(lambda x: get_alpha(x))
del data['tokens']
data['clean_tokens'] = data.alpha_tokens.apply(lambda x: cut_stopwords(x))
del data['alpha_tokens']

data.clean_tokens.apply(lambda x: colect_tokens(x))
print(all_tokens)

#data['TF'] = data.alpha_tokens.apply(lambda x: count_frequency(x))
texto = 'oi tudo bem? eu nao voto no bolsonaro'

tokens = remove_accents(texto)
tokens = word_tokenize(str(texto).lower(), language='portuguese')
tokens = get_alpha(tokens)
tokens = cut_stopwords(tokens)
print(FreqDist(tokens).most_common(200))
#Xablau

#------------------------------------------------------------------#
                         #SENTIMENT ANALYSIS#
#------------------------------------------------------------------#
def clean_tweet(tweet):
      return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|      (\w+:\/\/\S+)", " ", tweet).split())

def analize_sentiment(tweet):
    '''Translate the words from English into portuguese to get
    the feeling using english libraries'''
    analysis = TextBlob(clean_tweet(tweet))
    if analysis.detect_language() != 'en' and len(analysis)>=3:
        analysis = TextBlob(str(analysis.translate(to='en')))
        if analysis.sentiment.polarity > 0:
            return 1
        elif analysis.sentiment.polarity == 0:
            return 0
        else:
            return -1
    else:
        return 0

data['SA'] = np.array([ analize_sentiment(tweet) for tweet in data['Tweets'] ])

pos_tweets = [ tweet for index, tweet in enumerate(data['Tweets']) if data['SA'][index] > 0]
neu_tweets = [ tweet for index, tweet in enumerate(data['Tweets']) if data['SA'][index] == 0]
neg_tweets = [ tweet for index, tweet in enumerate(data['Tweets']) if data['SA'][index] < 0]
 
# Printing the percents of feelings:
 print("Percentage of positive tweets: {}%".format(len(pos_tweets)*100/len(data['Tweets'])))
print("Percentage of neutral tweets: {}%".format(len(neu_tweets)*100/len(data['Tweets'])))
print("Percentage de negative tweets: {}%".format(len(neg_tweets)*100/len(data['Tweets'])))

#Variable that will store all tweets with the chosen word in the API search function
api = twitter_setup()
public_tweets = api.search('bolsonaro')
 
#Variable that will store the polarities
analysis = None

for tweet in public_tweets:
      print(tweet.text)
      analysis = TextBlob(tweet.text)
      print(analysis.sentiment.polarity)

print('Average of feelings: ' + str(np.mean(analysis.sentiment.polarity)))