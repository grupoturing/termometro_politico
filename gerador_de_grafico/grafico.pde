class Grafico {
  String file_path = "";
  Table data;
  float atrito = 0.9;
  int x, y = 0;
  int h = 100;
  int w = 100;
  Ball[][] balls;
  Ball mouse;
  Grafico(int x, int y, int w, int h, float atrito) {
    this.atrito = atrito; 
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  void display(int b) {
    fill(200, 30, 30, 40);
    rectMode(CORNERS);
    rect(x, y, x+this.w, y+this.h);
    for (int i = b; i < balls.length; i++) {
      balls[0][i].display();
    }
  }

  void update(int b) {
    for (int j = b; j < balls.length; j++) {
      balls[0][j].update();
    }
    for (int j = b; j < balls.length; j++) {
      for (int i = j+1; i < balls.length; i++) {
        balls[0][j].checkCollision(balls[0][i]);
      }
      if (mousePressed){
      mouse.position.x = mouseX;
      mouse.position.y = mouseY;
      balls[0][j].checkCollision(mouse);
    }
    }
    
    
  }
  int import_data(String path) {
    file_path = path;
    data = loadTable(path, "header, tsv");
    println(data.getRowCount() + " palavra no dataset");
    
    int i = 0;
    for (TableRow row : data.rows()) {
      String palavra = row.getString("palavra");
      float freq = row.getFloat("frequencias");
      float sentimento = row.getFloat("sentimento")*0.5 +0.5;

      println(palavra + " sent of " + sentimento + " freq of " + freq);
      balls[0][i] = new Ball(x+sentimento*this.w,y+this.h/2/*y+random(0,this.h/4)+int(random(0.5,1.5))*this.h*3/4*/,  freq, x+sentimento*this.w,y+this.h/2, atrito, 0.5);
      i++;
    }
    mouse = new Ball(mouseX,mouseY,10,mouseX,mouseY,1,0);
    return data.getRowCount();
  }
  void modify(){
    if (mouse == null){
      mouse = new Ball(mouseX,mouseY,0.0005,mouseX,mouseY,1,0);
    }
  }
  void export() { //save data
    if(data.getColumnCount()<6){
      data.addColumn("cx");
      data.addColumn("cy");
      
    }
    int i = 0;
    for (TableRow row : data.rows()) {
      
      row.setFloat("cx", balls[0][i].position.x);
      row.setFloat("cy", balls[0][i].position.y);
      row.setFloat("r", balls[0][i].radius);
      i++;
    }

    saveTable(data, file_path );
  }
}
