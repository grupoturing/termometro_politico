from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.views.generic.edit import CreateView
from django.template import Context, loader


def index(request):
    return render(request, 'index.html', context=None)
    # return render(request, 'candidatos/t.html', context=None)


def grafico(request):
    return render(request, 'test.html', context=None)


def test(request):
    return render(request, 't2.html', context=None)


def getView(request):
    if request.method == 'GET':

        print("**get**")
        data = request.GET['myView']
        file_data = 'data/g1.tsv'
        template = loader.get_template("midias.html")
        sub_option = ""
        #                          áreas
        if(data == "candidatos"):
            file_data = "data/candidatos.tsv"
            template = loader.get_template("candidatos.html")
        elif(data == "eleitores"):
            file_data = "data/eleitores.tsv"
            template = loader.get_template("eleitores.html")
        elif(data == "midias"):
            file_data = "data/g1.tsv"
            sub_option = "Globo g1"
            template = loader.get_template("midias.html")

        #                         midias
        elif(data == "Uol"):
            file_data = "data/uol.tsv"
            sub_option = "Uol"
            template = loader.get_template("midias.html")
        elif(data == "G1"):
            file_data = "data/g1.tsv"
            sub_option = "Globo g1"
            template = loader.get_template("midias.html")
        elif(data == "Veja"):
            file_data = "data/veja.tsv"
            sub_option = "Veja"
            template = loader.get_template("midias.html")
        elif(data == "Estadão"):
            file_data = "data/estadao.tsv"
            sub_option = "Estadão"
            template = loader.get_template("midias.html")
        elif(data == "Folha"):
            file_data = "data/folha.tsv"
            sub_option = "Folha"
            template = loader.get_template("midias.html")

        #astr = "<html><b> Dados retornaram </b> <br> returned data: %s</html>" % send_data
        return HttpResponse(template.render({"data_path": file_data, "selected": sub_option}))
    return render(request)
