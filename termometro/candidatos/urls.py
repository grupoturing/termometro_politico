from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [

    path('', views.index, name='index'),
    path('grafico/', views.grafico, name='grafico'),
    path('test/', views.test, name='test'),
    path('grafico/getView', views.getView, name='getView'),


]
