
//             ------------------------ grafico ---------------------
function gerar() {

    var margin = {
            top: 20,
            right: 95,
            bottom: 10,
            left: 125
        },
        width = 400 - margin.left - margin.right,
        height,
        tickExtension = 20; // extend grid lines beyond scale range

    var formatPercent = d3.format(".0%"),
        formatTenthPercent = d3.format(".1%"),
        formatNumber = d3.format(",.3s"),
        formatDollars = function(d) {
            return (d < 0 ? "-" : "") + "$" + formatNumber(Math.abs(d)).replace(/G$/, "B");
        };

    var nameAll = "candidato X";

    var x = d3.scale.linear()
        .domain([0, .6])
        .rangeRound([0, width - 60])
        .clamp(true)
        .nice();

    var y0 = d3.scale.ordinal()
        .domain([nameAll])
        .range([150]);  // pnameAll text position

    var r = d3.scale.sqrt()
        .domain([0, 1e4])
        .range([0, 1]);

    var z = d3.scale.linear().domain([0,1]).range(["#cc2222","#229922"]);

    d3.selectAll(".g-graphic").each(function(d,i){console.log(1)});


    function getDivShape (div, st) {
        var width = d3.select(div)
          // get the width of div element
          .style(st)
          // take of 'px'
          .slice(0, -2)
        // return as an integer
        return Math.round(Number(width))};
    var w = getDivShape(".graph-container", 'width');
    var h = getDivShape(".graph-container", 'height');

    console.log(w);
    console.log(h);

    var chart_function = function(error, doc_palavras) {

        var candidato_x0 = w;
        var candidato_y0 = h;

        var sectors = d3.nest()
            .key(function(d) {
                return d.candidato;
            })
            .entries(doc_palavras);
            console.log("sectors: ");
        console.log(sectors);
        console.log(doc_palavras);

        var i;
        for( i = 0; i < sectors.length; i++){
            var s = [sectors[i]]
            console.log(sectors[i].key);
            console.log(s);

            var graph = d3.select("#graph-container_"+i).on("mouseenter", mouseenter_candidato(i))
            .on("mouseleave", mouseleave_candidato);


            console.log("#graph-container_"+(i));

            var svg = graph.selectAll(".g-graphic").append("svg")
            .attr("height", 400 + margin.top + margin.bottom)
            .attr("width", width + margin.left + margin.right)
            .attr("class", (sectors[i].key).replace(/ /g,"_"))
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


            console.log("svg: ");
            console.log(svg);

            var sector = svg.append("g")
            .attr("class", "g-sector")
            .selectAll("g")
            .data(s)
            .enter().append("g")
            .attr("transform", function(d) {
                return "translate(0,"+ 0 + ")";
            });
            console.log("sector");
            console.log(sector);
            var candP = sector.append("g")
                .attr("class", "g-sector-company")
                .selectAll("circle")
                .data(function(d) {
                    console.log(d.values);
                    return d.values;

                })
                .enter().append("circle")
                .attr("cx", function(d) {
                    return d.cx + candidato_x0/2;
                })

                .attr("cy", function(d) {
                    return d.cy;// + y0(nameAll);
                })
                .attr("r", function(d) {
                    return r(d.frequencia);
                })
                .style("fill", function(d) {
                    return isNaN(d.sentimento) ? null : z(d.sentimento);
                })
                .on("mouseover", mouseover)
                .on("mouseout", mouseout);

            var tip = graph.select("#g-tip_"+i);

            var tipMetric = tip.selectAll(".g-tip-metric")
                .datum(function() {
                    return this.getAttribute("data-name");
                });
        }
        console.log("ok");
        var container = d3.selectAll(".g-content");
        var candidatoPalavra = d3.selectAll("circle");
        var svg = d3.selectAll(".g-graphic");
        var tip = d3.selectAll(".g-tip");
        var tipMetric = tip.selectAll(".g-tip-metric");
        console.log("---");

        console.log("CandidatoPalavra");
        console.log(candidatoPalavra);

        console.log("tip");
        console.log(tip);
        console.log("tipMetric");
        console.log(tipMetric);
        console.log("container");
        console.log(container[0][0]);


        function mouseenter_candidato(value) {
            return function enter_candidato(){
                var count = 0;
                var cand_container = container.filter( function(d){
                    var state = count == value;
                    count++;
                    return state;
                }).classed("cand-active",true);
                cand_container.selectAll(".g-tip").classed("over_cand", true);
            }
        }

        function mouseleave_candidato() {
            var cand_container = container.filter(".cand-active").classed("cand-active", false);
            cand_container.selectAll(".g-tip").classed("over_cand", false);
        }


        var searchInput = d3.select(".g-search input")
            .on("keyup", keyuped);

        var searchClear = d3.select(".g-search .g-search-clear")
            .on("click", function() {
                searchInput.property("value", "").node().blur();
                search();
            });
        function keyuped() {
            if (d3.event.keyCode === 27) {
                this.value = "";
                this.blur();
            }
            search(this.value.trim());
        }

        function search(value) {
            if (value) {
                var re = new RegExp("\\b" + d3.requote(value), "i");
                svg.classed("g-searching", true);
                candidatoPalavra.classed("g-match", function(d) {
                    return re.test(d.palavra);// || re.test(d.candidato) ;
                });
                var matches = d3.selectAll(".g-match");
                console.log(matches[0]);
                if (matches[0].length === 1) mouseover(matches.datum());
                else mouseout();
                searchClear.style("display", null);
            } else {
                mouseout();
                svg.classed("g-searching", false);
                candidatoPalavra.classed("g-match", false);
                searchClear.style("display", "none");
            }
        }

        function mouseover(d) {
            console.log(d);
            candidatoPalavra.filter(function(c) {
                return c === d;
            }).classed("g-active", true);

            var dx, dy;
            dx = d.cx + candidato_x0/2, dy = d.cy;//+ y0(nameAll);

            dy -= 65, dx += 50; // margin fudge factors

            var tip_cand = tip.filter(".over_cand");
            tip_cand.style("display", null)
                .style("top", (dy - r(d.frequencia)) + "px")
                .style("left", dx + "px");

            tip_cand.select(".g-tip-title")
                .text( d.palavra);

            tipMetric.select(".g-tip-metric-value").text(function(name) {
                switch (name) {
                    case "frequencia":
                        return formatNumber(d.frequencia);
                    case "earnings":
                        return formatPercent(d.sentimento);
                }
            });
        }
        function mouseout() {
            tip.style("display", "none");
            candidatoPalavra.filter(".g-active").classed("g-active", false);
        }

    }


    //console.log(t);


    function frequencia(d) {
        return d.frequencia;
    }

    function sentimento(d) {
        return d.sentimento;
    }
    function type(d) {
        d.cx = +d.cx - 440;
        d.cy = +d.cy;
        //d.frequencia *= 1e6;
        d.sentimento *= 1;
        d.frequencia *=2;
        return d;
    }

};
