/*
 * @name Orbit Control
 * @description Orbit control allows you to drag and move around the world.
 */

var rotation = 0;
var cand;

var candidatos = new Array();


function setup() {

  var canvas = createCanvas(710, 400, WEBGL);

  // Move the canvas so it’s inside our <div id="sketch-holder">.
  canvas.parent('container');
  cand = new Candidato();

  for (var i = 0; i < 20; i++) {
    candidatos.push(new Candidato());
  }
}

function draw() {
  background(0, 0, 0, 0);
  var radius = width * 1.5;

  //drag to move the world.
  //orbitControl();
  normalMaterial();
  
  translate(0, 0, -600);
  for (var j = 0; j <= 10; j++) {
    push();
    var a = j/10 * PI+rotation;
    var b = 5/10 * PI;
    translate(sin(2 * a) * radius * sin(b), cos(b) * radius / 2, cos(2 * a) * radius * sin(b));
    translate(0, 0, -1000);

    candidatos[j].opacity = cos(2 * a) *255* sin(b);

    candidatos[j].display3D();

    //plane(max(cos(2 * a) *200* sin(b),0)+300);

    pop();
  }
  if (mouseX<width/3 && mouseX>0) {
    rotation += 0.01;
  }
  if (mouseX>width - width/3&& mouseX<width) {
    rotation -= 0.01;
  }
}
function Candidato() {
  this.pg = createGraphics(100, 150);
  this.pg.textSize(20);

  this.name = "candidato";
  this.opacity = 100;

  this.display = function() {
    this.pg.fill(200, 40, 40, this.opacity);
    this.pg.rect(0, 0, 50, 100);
    this.pg.tint(255, this.opacity);
    this.pg.text(this.name, 10, 50);

    texture(this.pg);
    plane(600);
  };
  this.display3D = function() {
    if (this.opacity > 160) {
      fill(200, 40, 40, this.opacity);
      box(400, 600, 1);
    }
  }
}
