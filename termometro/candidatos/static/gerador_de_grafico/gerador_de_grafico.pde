import java.util.Date;
Grafico g;
int b;
String[] files = {};
int files_index = 0;
void setup() {
  size(640, 360);
  
  frameRate(1000);
    // Using just the path of this sketch to demonstrate,
  // but you can list any directory you like.
  String path = sketchPath();
  path = path.substring(0,path.lastIndexOf('/') + 1)+"data/ok_folha";
  println(path);

  println("Listing all filenames in a directory: ");
  String[] filenames = listFileNames(path);
  
  ArrayList<File> allFiles = listFilesRecursive(path);

  for (File f : allFiles) {
    String parent = f.getParent().substring(f.getParent().lastIndexOf('/') + 1);
    String pre = trim(parent.substring(0,2));
    if(!f.isDirectory()){
      if(pre.equals("ok")){
        println("Name: " + f.getName());
        files = append(files, path+"/"+f.getName());
        println(files.length);
      }
    }
    /*
    println();
    println("Full path: " + f.getAbsolutePath());
    println("Is directory: " + f.isDirectory());
    println("Size: " + f.length());
    String lastModified = new Date(f.lastModified()).toString();
    println("Last Modified: " + lastModified);
    println("-----------------------");*/
  }
  println(files.length);
  g = new Grafico(0, 50, width, height-100, 0.7);
  b = g.import_data(files[files_index]);
  
}
int count = 0;
void draw() {
  background(0);
  g.display(b);
  g.update(b);
  if (b > 0) {
    count++;
    if (count ==2) {
      b --;
      count =0 ;
    }
  }
}
String file_name = "";
void keyPressed() {
  if (key == ENTER) {
    println("export");
    g.export();
  files_index+=1;
  if(files_index != files.length){
    g = new Grafico(0, 50, width, height-100, 0.7);
    b = g.import_data(files[files_index]);
  }
  }
  if (key == RETURN) {
    println("reset");
    g = new Grafico(0, 50, width, height-100, 0.7);
    b = g.import_data(files[files_index]);
  }
  if (key == 'z') {
    println("prev");
    if(files_index > 0)files_index--;
    g = new Grafico(0, 50, width, height-100, 0.7);
    b = g.import_data(files[files_index]);
  }
  if (key == 'a') {
    println("next");
    if(files_index > 0)files_index++;
    g = new Grafico(0, 50, width, height-100, 0.7);
    b = g.import_data(files[files_index]);
  }
}
// This function returns all the files in a directory as an array of Strings  
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}

// This function returns all the files in a directory as an array of File objects
// This is useful if you want more info about the file
File[] listFiles(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    File[] files = file.listFiles();
    return files;
  } else {
    // If it's not a directory
    return null;
  }
}

// Function to get a list of all files in a directory and all subdirectories
ArrayList<File> listFilesRecursive(String dir) {
  ArrayList<File> fileList = new ArrayList<File>(); 
  recurseDir(fileList, dir);
  return fileList;
}

// Recursive function to traverse subdirectories
void recurseDir(ArrayList<File> a, String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    // If you want to include directories in the list
    a.add(file);  
    File[] subfiles = file.listFiles();
    for (int i = 0; i < subfiles.length; i++) {
      // Call this function on all files in this directory
      recurseDir(a, subfiles[i].getAbsolutePath());
    }
  } else {
    a.add(file);
  }
}
