class Ball {
  PVector position;
  PVector velocity;
  PVector acc;
  PVector goal;
  float radius, m;
  float perda;
  float default_acc_x = 0.2;
  float default_acc_y = 0.1;
  float spring = 0.5;
  float padding = 2;
  Ball(float x, float y, float r_, float atraction_point_x,float atraction_point_y,float perda_, float padding_) {
    position = new PVector(x, y);
    velocity = new PVector(0,0);
    acc = new PVector(default_acc_x , y-atraction_point_y);
    acc.normalize();
    radius = sqrt(r_*5000)+1;
    padding = padding_;
    m = 1;
    perda = perda_;
    goal = new PVector(atraction_point_x, atraction_point_y );
    
  }

  void update() {
    position.add(velocity);
    velocity.add(acc);
    velocity.mult(perda);
    
    acc.x = default_acc_x;
    acc.y = default_acc_y;
    
    if (position.y-goal.y > 0) {
      acc.y = -acc.y;
    }
    
    if (position.x-goal.x > 0) {
      acc.x = -acc.x;
    }
    
  }

  void checkBoundaryCollision() {
    if (position.x > width-radius) {
      position.x = width-radius;
      velocity.x *= -1;
    } else if (position.x < radius) {
      position.x = radius;
      velocity.x *= -1;
    } else if (position.y > height-radius) {
      position.y = height-radius;
      velocity.y *= -1;
    } else if (position.y < radius) {
      position.y = radius;
      velocity.y *= -1;
    }
  }
  
  void checkCollision(Ball other) {
      float dx = other.position.x - position.x;
      float dy = other.position.y - position.y;
      float distance = sqrt(dx*dx + dy*dy);
      float minDist = other.radius + radius;
      if (distance < minDist+padding+padding*2) { 
        float angle = atan2(dy, dx);
        float targetX = position.x + cos(angle) * (minDist+padding*2);
        float targetY = position.y + sin(angle) * (minDist+padding*2);
        float ax = (targetX - other.position.x) * spring;
        float ay = (targetY - other.position.y) * spring;
        velocity.x -= ax;
        velocity.y -= ay;
        other.velocity.x += ax;
        other.velocity.y += ay;
      }
  }

  void display() {
    noStroke();
    fill(100+abs((position.x-goal.x)*3),100,100);
    ellipse(position.x, position.y, radius*2, radius*2);
  }
}
