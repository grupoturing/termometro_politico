import os

import re


def limpar(path, save_path):

    data = open(path).read()

    data = data.replace(" ,", " 0.")
    data = data.replace(" -,", " -0.")
    data = data.replace(",", ".")
    m = re.search(r'(\d)[^\d]*$', data)
    nome = data[m.start() + 2:-1]

    index = 0
    last_char = ' '
    new_data = ""

    for l in data.split('\n')[1:]:
        try:
            last_char = ' '
            l = l[l.find(' ') + 1:]
            print(l)
            if l[0].isdigit():
                l = l[l.find(' ') + 1:]
            if l[0].isdigit():
                continue
        except:
            continue
        index = 0
        new_line = ""
        for c in l:

            if c == ' ' and (l[index - 1].isdigit() or l[index + 1].isdigit()):
                new_line += '\t'
            else:
                new_line += c
            index += 1
        line_splited = new_line.split('\t')
        if len(line_splited) == 5:
            try:
                line_splited[2] = format(float(line_splited[2]), 'f')
                line_splited[3] = format(float(line_splited[3]), 'f')
            except:
                continue
            new_line = '\t'.join(line_splited)
            new_data += new_line
            new_data += '\n'

    print(new_data)
    print(nome)
    data = new_data.split('\n')[-1001:]
    new_data = "palavra\tocorencia\tfrequencia\tsentimento\tcandidato\n" + \
        '\n'.join(data)

    filename = save_path + nome.replace(' ', '_') + '.tsv'
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    with open(filename, 'w') as new_file:
        new_file.write(new_data)


path = 'folha/'

for root, dirs, files in os.walk(path):
    print(files)
    for f in files:
        print(f)
        limpar(path + f, "./ok_" + path)
